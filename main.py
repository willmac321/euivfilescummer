import sys
import os
from urllib.parse import urlparse
import shutil
import pickle

from threading import Timer
from PyQt6.QtGui import QGuiApplication, QIcon, QAction
from PyQt6.QtCore import QSize, QObject, pyqtSignal
from PyQt6.QtWidgets import QSystemTrayIcon, QMenu, QApplication
from PyQt6.QtQml import QQmlApplicationEngine
from PyQt6.QtQuick import QQuickWindow

class Backend(QObject):
    copyInSet = pyqtSignal(bool)
    copyInSuccess = pyqtSignal(bool)
    copyOutSuccess = pyqtSignal(bool)
    setSource = pyqtSignal(str)
    setDest = pyqtSignal(str)

    def __init__(self) -> None:
        super().__init__()
        self.sourceFP = None
        self.saveFileName =  "out.eu4"
        self.saveFP = None

    def connect(self, root):
        self.root = root
        root.setProperty('backend', self)
        root.source_accepted.connect(self.updateSource)
        root.dest_accepted.connect(self.updateDest)
        root.copy_out.connect(self.copyOut)
        root.copy_in.connect(self.copyIn)
        root.clear.connect(self.handleClear)
        # call this after ui is connected
        self.getFilePathsOut()

    def handleClear(self):
        if os.path.exists('dat'):
            os.remove('dat')
        self.sourceFP = None
        self.saveFileName =  "out.eu4"
        self.saveFP = None
        self.setSource.emit("...");
        self.setDest.emit("...");

    def checkCanScum(self):
        if not self.saveFileName or not self.saveFP:
            return
        if os.path.exists(os.path.join(self.saveFP, self.saveFileName)):
            self.copyInSet.emit(True)
        else:
            self.copyInSet.emit(False)

    def saveFilePathsOut(self):
        filePathObj = {'sourceFP':None if self.sourceFP is None else f"file://{self.sourceFP}", None if self.saveFP is None else 'saveFP':f"file://{self.saveFP}"}
        fileObj = open('dat', 'wb')
        pickle.dump(filePathObj, fileObj)
        fileObj.close()

    def getFilePathsOut(self):
        if os.path.exists('dat'):
            fileObj = open('dat', 'rb')
            filePathObj = pickle.load(fileObj)
            fileObj.close()
            if 'sourceFP' in filePathObj and filePathObj['sourceFP']:
                self.updateSource(filePathObj['sourceFP'])
            if 'saveFP' in filePathObj and filePathObj['saveFP']:
                self.updateDest(filePathObj['saveFP'])

    def updateSource(self, fp):
        source= urlparse(fp)
        self.saveFileName = os.path.basename(source.path)
        self.sourceFP = source.path
        self.checkCanScum()
        self.setSource.emit(fp);
        self.saveFilePathsOut()

    def updateDest(self, fp):
        source= urlparse(fp)
        self.saveFP = source.path
        self.saveFilePathsOut()
        self.checkCanScum()
        self.setDest.emit(fp);

    def copyOut(self):
        if self.sourceFP != None and self.saveFP != None:
            shutil.copy2(self.sourceFP, self.saveFP)
            self.copyInSet.emit(True)
            self.copyOutSuccess.emit(True)
            Timer(1.0, self.copyOutSuccess.emit, [False]).start()


    def copyIn(self):
        if self.sourceFP != None and self.saveFP != None:
            shutil.copy2(os.path.join(self.saveFP, self.saveFileName), os.path.dirname(self.sourceFP))
            self.copyInSuccess.emit(True)
            Timer(1.0, self.copyInSuccess.emit, [False]).start()

class Window():
    def __init__(self, parent=None) -> None:
        super().__init__()
        self.setIcon()

    def setIcon(self):
        icon = QIcon('./UI/scum.ico')
        icon.addFile('UI/icons/16x16.png', QSize(16,16))
        icon.addFile('UI/icons/24x24.png', QSize(24,24))
        icon.addFile('UI/icons/32x32.png', QSize(32,32))
        icon.addFile('UI/icons/48x48.png', QSize(48,48))
        icon.addFile('UI/icons/256x256.png', QSize(256,256))

        self.icon = icon

if __name__ == '__main__':
    QQuickWindow.setSceneGraphBackend('software')
    app = QApplication(sys.argv)

    engine = QQmlApplicationEngine()
    engine.quit.connect(app.quit)
    engine.load('./UI/main.qml')


    window = Window()

    root = engine.rootObjects()[0]
    backend = Backend()
    backend.connect(root)

    tray = QSystemTrayIcon()
    tray.setIcon(window.icon)
    tray.setVisible(True)

    menu = QMenu()
    quit = QAction("Exit")
    quit.triggered.connect(app.quit)
    menu.addAction(quit)

    tray.setContextMenu(menu)

    app.setWindowIcon(window.icon)
    sys.exit(app.exec())
