import QtQuick
import QtQuick.Controls
import QtQuick.Dialogs
import QtQuick.Layouts

ApplicationWindow {
    id: window

    property QtObject backend
    property string source_file: "..."
    property string dest_folder: "..."
    property bool canCopyIn: false
    property bool copyInSuccess: false
    property bool copyOutSuccess: false

    signal source_accepted(string fp)
    signal dest_accepted(string fp)
    signal copy_out()
    signal copy_in()
    signal set_source(string v)
    signal set_dest(string v)
    signal clear()

    visible: true
    width: 400
    height: 500
    title: "EUIV Save Scum"
    x: screen.desktopAvailableWidth - width
    y: screen.desktopAvailableHeight - height

    palette {
        disabled {
            button: "darkgrey"
        }

    }

    Image {
        anchors.fill: parent
        source: "portal.jpg"
        fillMode: Image.PreserveAspectCrop
    }

    Rectangle {
        opacity: 0.5
        anchors.fill: parent
        color: "black"
    }

    ColumnLayout {
        width: parent.width
        anchors.fill: parent
        spacing: 24
        Layout.leftMargin: 10
        Layout.rightMargin: 10

        ColumnLayout {
            width: parent.width
            spacing: 8

            Grid {
                Layout.leftMargin: 10
                Layout.rightMargin: 10
                columns: 2

                Label {
                    id: sourceLable

                    width: 90
                    text: "Source File: "
                }

                Label {
                    width: 260
                    wrapMode: Text.WrapAnywhere
                    text: source_file
                }

            }

            Button {
                Layout.alignment: Qt.AlignRight
                onClicked: fileDialogSource.open()

                Text {
                    anchors.centerIn: parent
                    text: "Save File"
                }

            }

            Grid {
                Layout.leftMargin: 10
                Layout.rightMargin: 10
                columns: 2

                Label {
                    width: 90
                    text: "Dest Folder: "
                }

                Label {
                    width: 260
                    wrapMode: Text.WrapAnywhere
                    text: dest_folder
                }

            }

            Button {
                Layout.alignment: Qt.AlignRight
                onClicked: folderDialogOut.open()

                Text {
                    anchors.centerIn: parent
                    text: "Copy Dest"
                }

            }

        }

        GridLayout {
            id: btn_grid
            Layout.preferredWidth: parent.width
            Layout.bottomMargin: 100
            Layout.leftMargin: 10
            Layout.rightMargin: 10
            Layout.alignment: Qt.AlignHCenter
            columns: 2

            Button {
                Layout.alignment: Qt.AlignHCenter
                id: btn_clear
                Layout.column: 0
                onClicked: clear()

                Text {
                    anchors.centerIn: parent
                    text: "Clear"
                }

            }

            ColumnLayout {
                Layout.column: 1
                Layout.alignment: Qt.AlignHCenter
                Button {
                    id: btn_saveFile

                    onClicked: copy_out()
                    enabled: !copyOutSuccess && source_file !== "..." && dest_folder !== "..."
                    icon.source: "check_48.ico"
                    display: copyOutSuccess ? AbstractButton.IconOnly : AbstractButton.TextOnly

                    Text {
                        anchors.centerIn: parent
                        text: copyOutSuccess ? "" : "Copy"
                        color: !enabled ? "lightgrey" : ""
                    }

                }
                Button {
                    id: btn_scum

                    onClicked: copy_in()
                    enabled: !copyInSuccess && canCopyIn && source_file !== "..." && dest_folder !== "..."
                    icon.source: "check_48.ico"
                    display: copyInSuccess ? AbstractButton.IconOnly : AbstractButton.TextOnly

                    Text {
                        anchors.centerIn: parent
                        text: copyInSuccess ? "" : "Scum"
                        color: !enabled ? "lightgrey" : ""
                    }

                }

            }

        }

    }

    FileDialog {
        id: fileDialogSource

        defaultSuffix: "eu4"
        nameFilters: ["EUIV files (*.eu4)"]
        onAccepted: {
            source_accepted(selectedFile);
        }
    }

    FolderDialog {
        id: folderDialogOut

        onAccepted: {
            dest_accepted(selectedFolder);
        }
    }

    Connections {
        function onCopyInSet(b) {
            canCopyIn = b;
        }

        function onCopyInSuccess(v) {
            copyInSuccess = v;
        }

        function onCopyOutSuccess(v) {
            copyOutSuccess = v;
        }

        function onSetSource(v) {
            source_file = v;
        }

        function onSetDest(v) {
            dest_folder = v;
        }

        target: backend
    }

}
